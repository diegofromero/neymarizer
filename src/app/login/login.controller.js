class LoginController {
  constructor($timeout, $log, $state, LoginService) {
    'ngInject';
    this.$log = $log;
    this.$state = $state;
    this.LoginService = LoginService;
    this.activate();
  }

  activate() {
    this.$log.debug('?');
  }

  login() {
    this.LoginService.authUser({
      email: this.username,
      password: this.password
    })
      .then((authData) => {
        this.$log.debug(authData);
        this.$state.go('main.home');
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

export default LoginController;
