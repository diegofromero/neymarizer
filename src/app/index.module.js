/* global malarkey:false, toastr:false, moment:false, Firebase:false */
import config from './index.config';

import routerConfig from './index.route';

import runBlock from './index.run';
/* Controllers */
import LoginController from './login/login.controller';
import MainController from './main/main.controller';
import HomeController from './home/home.controller.js';
import DeckController from './deck/deck.controller.js';
import DeckAddController from './deck/deck.add.controller.js';
import LeaderboardController from './leaderboard/leaderboard.controller.js';
import ProgressController from './progress/progress.controller.js';
/* Services */
import GithubContributorService from '../app/components/githubContributor/githubContributor.service';
import WebDevTecService from '../app/components/webDevTec/webDevTec.service';
import DeckService from '../app/components/deck/deck.service.js';
import LoginService from '../app/components/login/login.service.js';
/* Directives */
import NavbarDirective from '../app/components/navbar/navbar.directive';
import MalarkeyDirective from '../app/components/malarkey/malarkey.directive';

angular.module('footApp', ['ionic', 'firebase', 'ngAnimate', 'ngTouch', 'ngSanitize', 'restangular', 'ui.router'])
  .constant('malarkey', malarkey)
  .constant('toastr', toastr)
  .constant('moment', moment)
  .constant('REF', new Firebase("https://neymarize.firebaseio.com"))
  .config(config)

  .config(routerConfig)

  .run(runBlock)
  .service('githubContributor', GithubContributorService)
  .service('webDevTec', WebDevTecService)
  .service('DeckService', DeckService)
  .service('LoginService', LoginService)
  .controller('MainController', MainController)
  .controller('LoginController', LoginController)
  .controller('HomeController', HomeController)
  .controller('DeckController', DeckController)
  .controller('DeckAddController', DeckAddController)
  .controller('LeaderboardController', LeaderboardController)
  .controller('ProgressController', ProgressController)
  .directive('acmeNavbar', () => new NavbarDirective())
  .directive('acmeMalarkey', () => new MalarkeyDirective(malarkey));
