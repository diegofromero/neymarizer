(function () {
  'use strict';

  describe('login controller', function () {
    var $rootScope, $state, $injector, myServiceMock, vm;
    beforeEach(function () {
      module('footApp');

      inject(function(_$rootScope_, _$state_, _$injector_, $templateCache, $controller) {
        $rootScope = _$rootScope_;
        $state = _$state_;
        $injector = _$injector_;
        vm = $controller('LoginController');

      })
    });

    describe('user login', function ($controller) {
      var user = {};

      it('should validate login function as function', inject(function ($controller) {
        expect(angular.isFunction(vm.login)).toBeTruthy();
      }));

      it('should login sucessfully a user', inject(function ($controller, $location) {
        vm.login();
        expect($location.path).toEqual('/')
      }));

      it('should move logged user to home', inject(function ($controller) {
        vm.login();
        $rootScope.$apply();
        expect($state.current.name).toBe('main.home');
      }));
    });
  });
})();
