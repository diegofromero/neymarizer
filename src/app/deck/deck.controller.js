class DeckController {
  constructor($log, $stateParams, DeckService) {
    'ngInject';
    this.$log = $log;
    this.deckId = $stateParams.id;
    this.DeckService = DeckService;
    this.activate();
  }

  activate() {
    this.$log.debug(this.deckId);
    this.deck = this.DeckService.getDeck(this.deckId);
    this.requiredEquipment = this.DeckService.getDeckRequirementEquipment(this.deckId);
  }
}

export default DeckController;
