class ProgressController {
  constructor ($log, DeckService) {
    'ngInject';
    this.$log = $log;
    this.DeckService = DeckService;
    this.activate();
  }

  activate() {
    this.$log.debug('Home Controller');
    this.$log.debug('activated');
    this.decks = this.DeckService.getDecks();
  }
}

export default ProgressController;
