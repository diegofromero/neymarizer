class DeckService {
  constructor ($log, $http, REF, $firebaseArray, $firebaseObject) {
    'ngInject';
    this.decksREF = REF.child('decks');
    this.$http = $http;
    this.apiHost = 'https://api.github.com/repos/Swiip/generator-gulp-angular';
    this.$firebaseArray = $firebaseArray;
    this.$firebaseObject = $firebaseObject;
  }

  getDecks(){
    return this.$firebaseArray(this.decksREF);
  }

  getDeck(id){
    return this.$firebaseObject(this.decksREF.child(id));
  }

  getDeckRequirementEquipment(id){
    return this.$firebaseArray(this.decksREF.child(id).child('equipmentRequired'));
  }
}

export default DeckService;
