class LoginService {
  constructor($log, $q, $http, REF, $firebaseArray, $firebaseObject) {
    'ngInject';
    //this.decksREF = REF.child('decks');
    this.$http = $http;
    this.$firebaseArray = $firebaseArray;
    this.$firebaseObject = $firebaseObject;
    this.REF = REF;
    this.$q = $q;
  }

  authUser(authData) {
    const defer = this.$q.defer();
    console.log(authData);
    this.REF.authWithPassword({
      email: authData.email,
      password: authData.password
    }, (error, authData) => {
      if (error) {
        console.log("Login Failed!", error);
        defer.reject(error);
      } else {
        console.log("Authenticated successfully with payload:", authData);
        defer.resolve(authData);
      }
    });
    return defer.promise;
  }
}

export default LoginService;
