function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'app/login/login.html',
      controller: 'LoginController as login'
    })
    .state('main', {
      url: '/main',
      abstract: 'true',
      templateUrl: 'app/main/main.html', //this view contains our menu's
      controller: 'MainController as main'
    })
    .state('main.home', {
      //parent: 'main',
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'app/home/home.html',
          controller: 'HomeController as home'
        }
      }
    })
    .state('main.home.leaderboard', {
      //parent: 'main',
      url: '/leaderboard',
      views: {
        'tabContent': {
          templateUrl: 'app/leaderboard/leaderboard.html',
          controller: 'LeaderboardController as deck'
        }
      }
    })
    .state('main.home.progress', {
      //parent: 'main',
      url: '/progress',
      views: {
        'tabContent': {
          templateUrl: 'app/progress/progress.html',
          controller: 'ProgressController as deck'
        }
      }
    })
    .state('main.addDeck', {
      //parent: 'main',
      url: '/deck',
      views: {
        'menuContent': {
          templateUrl: 'app/deck/deck.add.html',
          controller: 'DeckAddController as deck'
        }
      }
    })
    .state('main.deck', {
      //parent: 'main',
      url: '/deck/:id',
      views: {
        'menuContent': {
          templateUrl: 'app/deck/deck.html',
          controller: 'DeckController as deck'
        }
      }
    })
  ;

  $urlRouterProvider.otherwise('/login');
}

export default routerConfig;
